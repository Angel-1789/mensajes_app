/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.util.Scanner;

/**
 *
 * @author Tito
 */
public class Inicio {
    
    
    public static void main(String[] args){
    
        Scanner sc = new Scanner(System.in);
        int opcion = 0;
        
        do{
            System.out.println("---------------------");
            System.out.println("Applicacion de mensajes");
            System.out.println("1. crear mensaje");
            System.out.println("2. listar mensajes");
            System.out.println("3. eliminar mensaje");
            System.out.println("4. editar mensaje");
            System.out.println("5. SALIR");
            
            // leer la opcion del usuario
            opcion = sc.nextInt();
            
            switch(opcion){
                case 1:
                    MensajesService.crearMensajes();
                    break;
                case 2:
                    MensajesService.listarMensajes();
                    break;
                case 3:
                    MensajesService.borrarMensajes();
                    break;
                case 4:
                    MensajesService.editarMensajes();
                    break;
                default:
                    break;
            }
            
        }while(opcion!=5);
      
        /*Conexion conexion = new Conexion();

        try(Connection cnx = conexion.getConnection()){
            System.out.println("HAY conexion");
        }
        catch(Exception e){
            System.out.println(e);
        }*/
    }
    
}
