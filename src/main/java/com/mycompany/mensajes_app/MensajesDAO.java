/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Tito
 */
public class MensajesDAO {
    
    public static void crearMensajeDB(Mensajes mensaje){
        Conexion db_connect = new Conexion();
        
        try(Connection conexion = db_connect.getConnection()){
            PreparedStatement ps = null;
            try{
                String query = "INSERT INTO mesajes (mensaje,autor_mensaje) VALUES (?,?)";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.executeUpdate(); // se encarga de envia la instruccion al DB para que se ejecute la query
                System.out.println("El mensaje fue creado correctamente");
            
            }catch(SQLException ex){
                System.out.println("error query: "+ex);
            }
        }catch(SQLException e){
            System.out.println("error coneccion: "+e);
        }
    }
    public static void leerMensajeDB(){
        Conexion db_connect = new Conexion();
        
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try(Connection conexion = db_connect.getConnection()){
            String query = "SELECT * FROM mesajes";
            ps = conexion.prepareStatement(query);
            rs = ps.executeQuery();
            
            while(rs.next()){
                System.out.println("id: "+rs.getInt("id_mensaje"));
                System.out.println("Mensaje: "+rs.getString("mensaje"));
                System.out.println("Autor: "+rs.getString("autor_mensaje"));
                System.out.println("Fecha: "+rs.getDate("fecha_mensaje"));
            }
        }catch(SQLException e){
            System.out.println("error coneccion");
            System.out.println(e);
        }
        
    }
    public static void borrarMensajeDB(int id_mensaje){
        Conexion db_connect = new Conexion();
        
        try(Connection conexion = db_connect.getConnection()){
            PreparedStatement ps = null;
            
            try{
                String query = "DELETE FROM mesajes WHERE mesajes.id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setInt(1, id_mensaje);
                ps.executeUpdate();
                System.out.println("El mensaje ha sido borrado");
            }catch(SQLException ex){
                System.out.println("Error Query: no se pudo borrar el mensaje");
                System.out.println(ex);
                
            }
        }catch(SQLException e){
            System.out.println("error coneccion");
            System.out.println(e);
        }
        
    }
    public static void actualizarMensajeDB(Mensajes mensaje){
        Conexion db_connect = new Conexion();
        
        try(Connection conexion = db_connect.getConnection()){
            PreparedStatement ps = null;
            
            try{
                String query = "UPDATE mesajes SET mensaje = ? WHERE mesajes.id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setInt(2, mensaje.getId_mensaje());
                ps.executeUpdate();
                System.out.println("El mensaje se actualizo correctamente");
            }catch(SQLException ex){
                System.out.println("Error Query: no se pudo actualizar el mensaje");
                System.out.println(ex);    
            }
        }catch(SQLException e){
            System.out.println("error conexion");
            System.out.println(e);
        }        
    } 
    
}
