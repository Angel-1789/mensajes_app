/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.util.Scanner;

/**
 *
 * @author Tito
 */
/** 
    Esta capa de servicios se encarga de recolectar los datos para envairlas a 
    la capa MensajesDAO.*/
public class MensajesService {
    
    public static void crearMensajes(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu mensaje: ");
        String mensaje = sc.nextLine();
        
        System.out.println("tu nombre: ");
        String nombre = sc.nextLine();
        
        Mensajes registro = new Mensajes();
        registro.setMensaje(mensaje);
        registro.setAutor_mensaje(nombre);
        
        // envio el objeto mensaje a la capa MensajeDAO
        MensajesDAO.crearMensajeDB(registro);
    }
    
    public static void listarMensajes(){
        MensajesDAO.leerMensajeDB();
        
    }
    public static void borrarMensajes(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Cual es el id del mensaje a borrar: ");
        int id = sc.nextInt();
        
        // envio el dato a MensajesDAO
        MensajesDAO.borrarMensajeDB(id);
        
    }
    public static void editarMensajes(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu nuevo mensaje: ");
        String mensaje = sc.nextLine();
        
        System.out.println("Cual es el id del mensaje a editar: ");
        int id = sc.nextInt();
        
        Mensajes actualizacion = new Mensajes();
        actualizacion.setId_mensaje(id);
        actualizacion.setMensaje(mensaje);
               
        // envio el dato a MensajesDAO
        MensajesDAO.actualizarMensajeDB(actualizacion);
    }
    
    
}
